import axios from 'axios'
import config from './config'

const api = axios.create({
    baseURL: config.BASE_URL
})

api.interceptors.request.use(function (config) {

    return config
})

api.interceptors.response.use(
    response => {
        return response
    }
)

export default api