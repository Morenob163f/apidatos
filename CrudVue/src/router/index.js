import Editar from '../../views/Editar.vue'

const router = new VueRouter({
  routes: [
    
    {
      path: '/editar/:id',
      name: 'Editar',
      component: { Editar: '<h1> Your id is {{$route.params.id}} </h1>' }
    }
  ]
});

