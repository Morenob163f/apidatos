<?php
        /**
        * Clases para conectarse a manejadores de base de datos por odbc
        * probado en postgresql y sql server 
        */
        class OdbcConnection {
                private $_hConnection;
                private $_sConnectionString;

                function __construct($ConnectionString = "") {
                        $this->_hConnection = NULL;
                        $this->_sConnectionString = $ConnectionString;
                }

                function __destruct() {
                        if($this->_hConnection) {
                                $this->close();
                        }
                        unset($this->_sConnectionString);
                }
                
                function beginTransaction() {
                        if(!$this->_hConnection)
                                throw new Exception("No existen conexiones abiertas.");
                        if(!odbc_autocommit($this->_hConnection, FALSE))
                                throw new Exception("No se pudo iniciar la transaccion. " . odbc_errormsg($this->_hConnection));
                }

                function close() {
                        if($this->_hConnection) {
                                odbc_close($this->_hConnection);
                                $this->_hConnection = NULL;
                        }
                }
                
                function createCommand() {
                        $cmd = new OdbcCommand();
                        $cmd->setConnection($this);
                        return $cmd;
                }

                function getConnection() {
                        return $this->_hConnection;
                }

                function open() {
                        $this->_hConnection = odbc_connect($this->_sConnectionString, '', '');
                        if(!$this->_hConnection)
                                throw new Exception("Error al intentar conectarse al servidor. Codigo de error [" . odbc_error(). "] " . odbc_errormsg());
                }

                function setConnectionString($ConnectionString) {
                        $this->_sConnectionString = $ConnectionString;
                }
        }

        class OdbcCommand {
                private $_hConnection;
                private $_sCommandText;

                function __construct() {
                        $this->_sCommandText = "";
                }

                function __destruct() {
                        $this->_sCommandText = "";
                }

                function commit() {
                        if(!$this->_hConnection->getConnection())
                                throw new Exception("La conexion esta cerrada.");
                        if(odbc_autocommit($this->_hConnection->getConnection()))
                                throw new Exception("No existe transacción activa.");
                        if(!odbc_commit($this->_hConnection->getConnection()))
                                throw new Exception("Error al ejecutar la consulta. " . odbc_errormsg($this->_hConnection->getConnection()));
                }

                function executeDataSet($AssociativeOnly = TRUE) {
                        if(!$this->_hConnection->getConnection())
                                throw new Exception("La conexion esta cerrada.");
                        if($this->_sCommandText == "")
                                throw new Exception("El texto de la consulta no puede estar vacío.");
                        $resultSet = odbc_exec($this->_hConnection->getConnection(), $this->_sCommandText);
                        if(!$resultSet)
                                throw new Exception("Error al ejecutar la consulta. " . odbc_errormsg($this->_hConnection->getConnection()));
                        $dataset = array();
                        while($row = odbc_fetch_array($resultSet)) {
                                foreach ($row as $key => $value) {
                                        if(!$AssociativeOnly)
                                                $tmparray[] = $value;
                                        $tmparray[$key] = $value;
                                }
                                array_push($dataset, $tmparray);
                                unset($tmparray);
                                unset($row);
                        }
                        odbc_free_result($resultSet);
                        return $dataset;
                }
                
                function executeNonQuery() {
                        if(!$this->_hConnection->getConnection())
                                throw new Exception("La conexion esta cerrada.");
                        if($this->_sCommandText == "")
                                throw new Exception("El texto de la consulta no puede estar vacío.");
                        $resultSet = odbc_exec($this->_hConnection->getConnection(), $this->_sCommandText);
                        if(!$resultSet)
                                throw new Exception("Error al ejecutar la consulta. " . odbc_errormsg($this->_hConnection->getConnection()));
                        $rowCount = odbc_num_rows($resultSet);
                        odbc_free_result($resultSet);
                        return $rowCount;
                }
                
                function rollback() {
                        if(!$this->_hConnection->getConnection())
                                throw new Exception("La conexion esta cerrada.");
                        if(odbc_autocommit($this->_hConnection->getConnection()))
                                throw new Exception("No existe transacción activa.");
                        if(!odbc_rollback($this->_hConnection->getConnection()))
                                throw new Exception("No se pudo cancelar la transaccion. " . odbc_errormsg($this->_hConnection->getConnection()));
                }

                function setCommandText($CommandText) {
                        $this->_sCommandText = trim($CommandText);
                }

                function setConnection($OdbcConnection) {
                        if(!$OdbcConnection instanceof OdbcConnection)
                                throw new Exception("El parámetro recibido no es de tipo OdbcConnection");
                        $this->_hConnection = $OdbcConnection;
                }
        }
