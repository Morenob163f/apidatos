<?php      
    include_once('./utils/odbcclient.php');
 
class GuardarArticulo{
        public function GuardarArticulos($data) 
        {                   
            global $ConnectionString, $output;
            $ds = null;      
            try {                   
             /*   foreach($data as $row) {             
                $codigo =  $row[0]["codigo"];                        
                $nombre = $row[0]["nombre"];                        
                $categoria = $row[0]["categoria"];                        
                $descripcion = $row[0]["descripcion"];                        
                }   */ 
                $codigo =  $data["codigo"];                        
                $nombre = $data["nombre"];                        
                $categoria = $data["categoria"];                        
                $descripcion = $data["descripcion"];                                            
                // $mynewstring = str_replace(" \" ", " ", $stringArticulo, $count);           
                $query = "dbo.GuardarArticulos " . "'". $codigo. "'". ",". "'".$nombre. "'". ",".$categoria.",". "'".$descripcion."'";  
                try {
                    $conn = new OdbcConnection();
                    $conn->setConnectionString($ConnectionString['conexiondb']);
                    $conn->open();
                    //$conn->beginTransaction(); roll back
    
                    $cmd = $conn->createCommand();
                    $rollback = FALSE;
                    $cmd->setCommandText($query);
                    $ds = $cmd->executeDataSet();
                    $conn->close();
                } catch (Exception $e) {
            
                }

            }
            catch(Exception $ex) {
                throw new restfulException(1, "Error interno en el servicio", 500);
            }                    
            echo json_encode($ds);
        }
        public function EditarArticulos($data) 
        {              
            //echo json_encode($data);       
            global $ConnectionString, $output;
            $ds = null;      
            try {                                                                                                  
                $query = "dbo.EditarArticulos"." " .$data["id"] . ",". "'". $data["codigo"]. "'". ",". "'".$data["nombre"]. "'". ",".$data["categoria"].",". "'".$data["descripcion"]."'";     
                //echo json_encode($query);               
                try {
                    $conn = new OdbcConnection();
                    $conn->setConnectionString($ConnectionString['conexiondb']);
                    $conn->open();          
                    $cmd = $conn->createCommand();
                    $rollback = FALSE;
                    $cmd->setCommandText($query);
                    $ds = $cmd->executeDataSet();
                    $conn->close();
                } catch (Exception $e) {
            
                }

            }
            catch(Exception $ex) {
                throw new restfulException(1, "Error interno en el servicio", 500);
            }                    
            echo json_encode($ds);
        }
        public function BorrarArticulos($data)
        {           
            global $ConnectionString, $output;
            $ds = null;      
            try {                                                                            
                $query = "dbo.BorrarArticulos" ." " .$data["id"];   
               // echo json_encode($query);       
                try {
                    $conn = new OdbcConnection();
                    $conn->setConnectionString($ConnectionString['conexiondb']);
                    $conn->open();      
                    $cmd = $conn->createCommand();
                    $rollback = FALSE;
                    $cmd->setCommandText($query);
                    $ds = $cmd->executeDataSet();
                    $conn->close();
                } catch (Exception $e) {
            
                }
                $articulos = array();
          
                foreach($ds as $row) {
                        $newRow = array();
                        $newRow["id"] = $row["id"];
                        $newRow["codigo"] = $row["codigo"];
                        $newRow["nombre"] = $row["nombre"];
                        $newRow["categoria"] = $row["categoria"];
                        $newRow["descripcion"] = $row["descripcion"];
                        array_push($articulos, $newRow);
                }        
             
            }
            catch(Exception $ex) {
                throw new restfulException(1, "Error interno en el servicio", 500);
            }                    
            echo json_encode($articulos);
        }
        public function ObtenerAticulo($data) 
        {                             
            global $ConnectionString, $output;
            $ds = null;      
            try {                                                                                                  
                $query = "dbo.ObtenerArticulos" ." " .$data["id"];                       
                try {
                    $conn = new OdbcConnection();
                    $conn->setConnectionString($ConnectionString['conexiondb']);
                    $conn->open();          
                    $cmd = $conn->createCommand();
                    $rollback = FALSE;
                    $cmd->setCommandText($query);
                    $ds = $cmd->executeDataSet();
                    $conn->close();
                } catch (Exception $e) {
            
                }
                $articulo = array();
          
                foreach($ds as $row) {
                    $newRow = array();
                    $newRow["id"] = $row["id"];
                    $newRow["codigo"] = $row["codigo"];
                    $newRow["nombre"] = $row["nombre"];
                    $newRow["categoria"] = $row["categoria"];
                    $newRow["descripcion"] = $row["descripcion"];
                    array_push($articulo, $newRow);
            } 

            }
            catch(Exception $ex) {
                throw new restfulException(1, "Error interno en el servicio", 500);
            }                    
            echo json_encode($articulo);
        }
    }
?>