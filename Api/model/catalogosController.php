<?php          
    include_once('./utils/odbcclient.php');
    class categoria 
    {                        
        public function getCategoria() 
        {                   
            global $ConnectionString, $output;
            $ds = null;
            try {
                    $conn = new OdbcConnection();
                    $conn->setConnectionString($ConnectionString['conexiondb']);
                    $conn->open();
                    $cmd = $conn->createCommand();
                    $cmd->setCommandText("dbo.ObtenerCategorias");
                    $ds = $cmd->executeDataSet();
                    $conn->close();
            }
            catch(Exception $ex) {
                throw new restfulException(1, "Error interno en el servicio", 500);
            }
            $categorias = array();
          
            foreach($ds as $row) {
                    $newRow = array();
                    $newRow["id"] = $row["id"];
                    $newRow["Nombre"] = utf8_encode($row["Nombre"]);
                    array_push($categorias, $newRow);
            }        
            echo json_encode($categorias);
        }
    }
    class articulos{
        public function getArticulos() 
        {                   
            global $ConnectionString, $output;
            $ds = null;
            try {
                    $conn = new OdbcConnection();
                    $conn->setConnectionString($ConnectionString['conexiondb']);
                    $conn->open();
                    $cmd = $conn->createCommand();
                    $cmd->setCommandText("dbo.ObtenerArticulos");
                    $ds = $cmd->executeDataSet();
                    $conn->close();
            }
            catch(Exception $ex) {
                throw new restfulException(1, "Error interno en el servicio", 500);
            }
            $articulos = array();
          
            foreach($ds as $row) {
                    $newRow = array();
                    $newRow["id"] = $row["id"];
                    $newRow["codigo"] = $row["codigo"];
                    $newRow["nombre"] = $row["nombre"];
                    $newRow["categoria"] = $row["categoria"];
                    $newRow["descripcion"] = $row["descripcion"];
                    array_push($articulos, $newRow);
            }        
            echo json_encode($articulos);
        }
    }  
?>